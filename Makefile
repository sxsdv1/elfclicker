ICONS = $(wildcard content/icons/*.svg)
ASSETS := \
	content/icons.svg \
	content/Stontex64.png \
	content/icons/glass-shot.svg \
	content/icons/drop.svg \
	content/icons/splurt.svg \
	content/icons/slap.svg \
	content/elf0.png \
	content/elf1.png \
	content/elf2.png \
	content/elf3.png

elfclicker.zip: index.html default.css $(ASSETS) src/*
	zip -o $@ $^

content/icons.svg: $(ICONS)
	svgstore -o $@ $^

content/Stontex64.png: content/Stontex.png
	convert -scale 64 $< $@

content/%.png: asset-hack/%.svg
	inkscape --export-type png --export-dpi 360 -o $@ $<

asset-hack/elf0.svg: asset-hack/vector-elf.svg
	cp $< $@

asset-hack/elf1.svg: asset-hack/vector-elf.svg
	sed 's/#b28e57/#ffe0bd/g; s/#d2aa70/#dd5555/g' $< > $@

asset-hack/elf2.svg: asset-hack/vector-elf.svg
	sed 's/#b28e57/#ffcd94/g; s/#d2aa70/#f9e2e2/g' $< > $@

asset-hack/elf3.svg: asset-hack/vector-elf.svg
	sed 's/#b28e57/#e0ac69/g; s/#d2aa70/#0cb0ab/g' $< > $@
