// Collection of sample scenarios to catch unexpected effects when changing
// logic and some sanity tests

import test from 'ava'
import Elf from './src/elf.js'
import {sample, run} from './sample.js'
import immutable from './src/immutable.js'
const {Set} = immutable

const SAMPLES = 100
const rootState = {
    unlocked: Set()
}

test('not-controlled', t => {
    let stats = sample('not-controlled', SAMPLES, () => {
        let initState = new Elf()
        let state = run(rootState, initState, 1000)

        t.assert(state.spilledCum > state.spilledPrecum, "More cum than precum")
        if (state.action != 'masturbate') {
            t.is(state.orgasms, state.masturbated)
        }
        return state
    })
    t.assert(stats.juice.mean < 35, "Takes care of things before build up")
    t.assert(stats.juice.max < 55, "Takes care of things before build up")
    t.assert(stats.dripped.mean > 40 && stats.dripped.mean < 100)
    t.assert(stats.spilledCum.min > 200 && stats.spilledCum.max < 550)
    t.assert(stats.spilledCum.mean > 250 && stats.spilledCum.mean < 500, "Should be some cum")
    t.assert(stats.spilledPrecum.min > 19 && stats.spilledPrecum.max < 200)
    t.assert(stats.spilledPrecum.mean > 40 && stats.spilledPrecum.mean < 100)
    t.assert(stats.cumshots.mean > 20 && stats.cumshots.mean < 50)
    t.assert(stats.masturbated.mean >= 4 && stats.masturbated.mean <= 6)
})

test('max-stroke', t => {
    let stats = sample('max-stroke', SAMPLES, () => {
        let initState = (new Elf()).setAction('stroke', 1000)
        return run(rootState, initState, 1000)
    })
    t.assert(stats.orgasms.mean >= 7 && stats.orgasms.mean < 9)
    t.assert(stats.orgasms.stddev < 1)
    t.assert(stats.cumshots.mean > 30 && stats.cumshots.mean < 40)
    t.assert(stats.collectedCum.mean > 330 && stats.collectedCum.mean < 350)
    t.assert(stats.collectedPrecum.mean > 15 && stats.collectedPrecum.mean < 50)
})

test('max-tease', t => {
    let stats = sample('max-tease', SAMPLES, () => {
        let initState = (new Elf()).setAction('tease', 1000)
        return run(rootState, initState, 1000)
    })
    t.assert(stats.orgasms.mean > 1 && stats.orgasms.mean < 4)
    t.assert(stats.collectedPrecum.mean > 600 && stats.collectedPrecum.mean < 900)
    t.assert(stats.collectedPrecum.max < 1000)
    t.assert(stats.collectedCum.mean > 200 && stats.collectedCum.mean < 350)
})

test('max-finger', t => {
    let stats = sample('max-finger', SAMPLES, () => {
        let initState = (new Elf()).setAction('finger', 1000)
        return run(rootState, initState, 1000)
    })
    t.assert(stats.orgasms.mean > 4 && stats.orgasms.mean < 7)
    t.assert(stats.cumshots.mean > 25  && stats.cumshots.mean < 35)
    t.assert(stats.collectedPrecum.mean > 35 && stats.collectedPrecum.mean < 500)
    t.assert(stats.collectedPrecum.max < 700)
    t.assert(stats.collectedCum.mean > 380 && stats.collectedCum.mean < 450)
})

test('max-edge', t => {
    let stats = sample('max-edge', SAMPLES, () => {
        let initState = (new Elf()).setAction('edge', 1000)
        return run(rootState, initState, 1000, (state) => {
            t.assert(state.action == 'idle' || state.action == 'edge')
            if (state.action == 'idle') {
                if (state.climax < 60) {
                    return state.setAction('edge', 1000)
                }
            }
            return state
        })
    })
    t.assert(stats.orgasms.mean > 4 && stats.orgasms.mean < 7)
    t.assert(stats.orgasms.stddev > 0.4 && stats.orgasms.stddev < 3)
    t.assert(stats.edges.mean > 11 && stats.edges.mean < 16)
    t.assert(stats.cumshots.mean > 22 && stats.cumshots.mean < 30)
    t.assert(stats.collectedCum.mean > 320 && stats.collectedCum.mean < 405)
    t.assert(stats.collectedPrecum.mean > 170 && stats.collectedCum.mean < 400)
    t.assert(stats.collectedPrecum.max < 1000);
})

test('idle-chastity', t => {
    let stats = sample('idle-chastity', SAMPLES, () => {
        let initState = new Elf({ chastityCage: true })
        return run(rootState, initState, 1000)
    })
    t.assert(stats.orgasms.mean > 0 && stats.orgasms.mean < 1)
    t.assert(stats.cumshots.mean >= 0 && stats.cumshots.mean < 5)
})
