import fs from 'fs'
import {mean, standardDeviation} from 'simple-statistics'

let GIT_COMMIT = process.env['GIT_COMMIT']

export let run = (rootState, state, ticks, callback) => {
    let stats = {
        orgasms: 0,
        cumshots: 0,
        dripped: 0,
        masturbated: 0,
        edges: 0,
        timeToCum: 0,
    }
    for (let tick = 1; tick < ticks; tick++) {
        let newState = state.tick(rootState)
        if (callback) {
            newState = callback(newState)
        }
        if (newState.cumStep > 0) {
            if (state.cumStep == 0) {
                stats.orgasms++
            }
            if (stats.timeToCum == 0) {
                stats.timeToCum = tick
            }
            stats.cumshots++
        }
        if (newState.spilledPrecum > state.spilledPrecum || newState.collectedPrecum > state.collectedPrecum ) {
            stats.dripped++
        }
        if (newState.action == 'masturbate' && state.action != 'masturbate') {
            stats.masturbated++
        }
        if (newState.action == 'edge' && state.action != 'edge') {
            stats.edges++
        }
        state = newState
    }
    return {
        juice: state.juice,
        dripped: state.dripped,
        spilledCum: state.spilledCum,
        spilledPrecum: state.spilledPrecum,
        collectedCum: state.collectedCum,
        collectedPrecum: state.collectedPrecum,
        action: state.action,
        ...stats
    }
}

let pick = (objects, key) =>
    objects.map(obj => obj[key])

export let sample = (name, count, fun) => {
    let samples = []
    while (count--) {
        samples.push(fun())
    }
    fs.writeFile(`samples/${name}-${GIT_COMMIT}.json`, JSON.stringify(samples), err => {
        if (err) {
            console.log(err)
        }
    })
    let reference = samples[0]
    let result = Object.fromEntries(
        Object.keys(reference)
            .map((key) => {
                let values = samples.map(obj => obj[key])
                return [key, {
                    mean: mean(values),
                    stddev: standardDeviation(values),
                    min: Math.min(...values),
                    max: Math.max(...values),
                }]
            })
    )
    return result
}
