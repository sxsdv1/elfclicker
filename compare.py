import argparse
import json
import math
import numpy
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon


colors = (
    '#d33682',
    '#b58900',
    '#cb4b16',
    '#dc322f',
    '#6c71c4',
    '#268bd2',
    '#2aa198',
    '#859900',
)

def mangle_name(name: str) -> str:
    last = name.rsplit('/', 1)[-1]
    return last[:32]


def show_stats(samples) -> None:
    keys = list(sorted(set(samples[0][1][0].keys()) - {'action'}))
    names = [mangle_name(name) for name, _ in samples]
    print(', '.join(names))
    cols = min(len(keys), 4)
    rows = math.ceil(len(keys) / cols)
    fig, axs = plt.subplots(rows, cols)
    width = 0.35
    show_legend = True
    for key, ax in zip(keys, [ax for row in axs for ax in row]):
        ax.set_xlabel(key)
        bp = ax.boxplot([[d[key] for d in data] for _, data in samples])

        # Patch on color
        patches = []
        for box, color in zip(bp['boxes'], colors):
            boxX = []
            boxY = []
            for i in range(5):
                boxX.append(box.get_xdata()[i])
                boxY.append(box.get_ydata()[i])
            boxcoords = numpy.column_stack([boxX, boxY])
            patches.append(ax.add_patch(Polygon(boxcoords, facecolor=color)))
        if show_legend:
            fig.legend(patches, names)
            show_legend = False

    plt.show()


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('file', nargs='*', type=argparse.FileType('r'))

    args = parser.parse_args()
    data = [(file.name, json.load(file)) for file in args.file]
    show_stats(data)


if __name__ == '__main__':
    main()
