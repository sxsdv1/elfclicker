let cloneChildItem = (el) => {
    let reference = el.firstElementChild
    let cloned = reference.cloneNode(true)
    let before = reference.nextElementSibling
    while (before && !before.matches('.after-items')) {
        before = before.nextElementSibling;
    }
    if (before) {
        reference.parentNode.insertBefore(cloned, before)
    } else {
        reference.parentNode.appendChild(cloned)
    }
    return cloned
}

export let getDataBindings = (el) =>
    el.dataset.bind.split(',')
        .map(bind => {
            let [attr, key] = bind.split(':')
            return { el, attr, key }
        })

export let findDataBindings = (root) => {
    let elements = [
        ...(root.matches && root.matches('[data-bind]') ? [root] : []),
        ...root.querySelectorAll('[data-bind]')
    ]
    let bindings = elements.flatMap(getDataBindings)
    let allItemBindings = []
    bindings.filter(bind => bind.attr == 'items')
        .forEach(bind => {
            bind.items = []
            bind.el.querySelectorAll('[data-key]').forEach(item => {
                let itemBindingElements = [item, ...item.querySelectorAll('[data-bind]')]
                let itemBindings = bindings.filter(bind => itemBindingElements.indexOf(bind.el) !== -1)
                bind.items.push(itemBindings)
                allItemBindings.push(...itemBindings)
            })
        })
    return bindings.filter(bind => allItemBindings.indexOf(bind) == -1)
}

export let syncDisplayProperties = (bindings, oldProps, newProps) => {
    if (!newProps) {
        throw new Error("newProps must be set")
    }
    for (let {el, context, attr, key, items} of bindings) {
        let value = newProps[key]
        if (value === undefined) {
            throw new Error(`No such property ${key}`)
        }
        if (oldProps) {
            if (value == oldProps[key]) {
                continue
            }
        }
        if (attr === 'text') {
            el.textContent = value
        } else if (attr === 'display') {
            el.style.display = value ? null : 'none'
        } else if (attr === 'enabled') {
            el.disabled = !value
        } else if (attr === 'items') {
            while (items.length < value.size) {
                let newChildItem = cloneChildItem(el)
                items.push(findDataBindings(newChildItem))
            }
            while (items.length > value.size) {
                items.shift()
                el.firstElementChild.remove()
            }
            items.forEach((item, i) => {
                syncDisplayProperties(item, oldProps?.[key].get(i), value.get(i))
            })
        } else if (attr.startsWith("data-")) {
            el.dataset[attr.substring(5)] = value
        } else if (attr.startsWith("style-")) {
            el.style.setProperty(attr.substring(6), value)
        } else {
            el[attr] = value
        }
    }
}
