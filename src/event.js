let asMap = (collection) => Object.fromEntries(collection.map(obj => [obj.id, obj]))
let createTag = (mapData, mapResult) => (literals, ...data) =>
    mapResult(literals.reduce((acc, literal, i) => acc + mapData(data[i - 1]) + literal))

let paragraphs = createTag(d => d, r => {
    let paras = r.split('\n\n')
    return (doc) => paras.map(para => {
        let el = doc.createElement('p')
        el.textContent = para
        return el
    })
})

export default asMap([
    {
        id: 'cumlubeOrder1',
        name: "Special Order - Cumlube",
        target: { cumlube: 2 },
        reward: { coins: 150 },
        introContent: paragraphs`
            One of the girls working in the brothel for Madam Murbul comes by
            and asks if you could help provide cumlube for a upcoming gang bang
            event.
        `,
        descriptionText: "Deliver 2 bottles of cumlube to Madam Murbul",
        completeContent: paragraphs`
            Madam Murbul looks the bottle of cumlube over.

            "Yes, this will do" she says and hands you a couple of big gold coins.

            Got 150 coins.
        `
    },
    {
        id: 'cumlubeOrder2',
        name: "Special Order - Cumlube",
        after: [ 'cumlubeOrder1' ],
        target: { cumlube: 1 },
        unlocks: ['chastityCage'],
        reward: { chastityCage: 1, coins: 75 },
        introContent: paragraphs`
            Zarod, the village blacksmith, enters your shop and explains he
            needs to get some extra slippery lube. Appearently his wife is much
            to tight for his "massive dong".

            Hard to believe considering she's a mother of 5, but he promises to pay well.
        `,
        descriptionText: "Deliver a bottle of cumlube to Zarod the blacksmith",
        completeContent: paragraphs`
            Zarod removes the cap from a bottle of cumlube and sticks a grubby
            finger in and says "Yeah, that's the stuff! With this on my rod I
            don't think the missus is going to mind me trying to push it up her
            bum anymore!"

            You shake your head

            "Oh, right.. payment" says Zarod and throws a full pouch of coins
            on the counter "I think that should be enough for your trouble but
            I also found this old *ehem* custom order that was never collected
            in my smithy. I think you can put it to good use and let me know if
            you need more of them

            Got 75 coins and a chastity cage

            Chastity cage unlocked.
        `
    },
    {
        id: 'cumlubeOrder3',
        name: "Special Order - Cumlube",
        after: [ 'cumlubeOrder1' ],
        target: { cumlube: 5 },
        unlocks: ['edge'],
        reward: { coins: 500 },
        introContent: paragraphs`
            The cumlube you provided for gang bang at Madam Murbul's brothel
            was a huge hit and the Madam herself enters your shop to place a
            big order.
        `,
        descriptionText: "Deliver 5 bottles of cumlube to Madam Murbul",
        completeContent: paragraphs`
            "Excellent!" exclaims Murbul as you enter the doors of the brothel
            to deliver the cumlube.

            "My girls refuse to work with anything else now and the clients
            leave more satisfied then ever"

            She takes one of the bottles and walks over to a orc girl in the
            corner with both hands around the massive prick of a customer.

            "Sharn here got a special technique that can keep a cock on the
            edge of bursting until even the hardest warrior is begging to cum"

            She dribble a bit of cumlube over his throbbing rod.

            "With a bit this he will soon cum like a fountain"

            The orc girl keeps working the now lubed up cock meat as tears
            start running from the orc warriors eyes and precum from his girthy
            cock until finally he erupts with a shout.

            You think you could put some of her technique to good use yourself
            back at the shop.

            Got 500 coins.

            Edge action unlocked.
        `
    },
    {
        id: 'cumlubeOrder4',
        name: "Special Order - Cumlube",
        after: [ 'cumlubeOrder3' ],
        target: { cumlube: 5 },
        unlocks: ['finger'],
        reward: { coins: 500 },
        introContent: paragraphs`
            One of Madam Murbul's girls comes in with another order for 5
            bottles of cumlube.
        `,
        descriptionText: "Deliver 5 bottles of cumlube to Madam Murbul",
        completeContent: paragraphs`
            "We're running through our lube supply so quickly these days" says
            the Madam as she hands over a bag of coins in payment.

            "But business has never been better"

            "Say, would you like Sharn to show you another of her special cock milking techniques?"

            She leads you to backroom where you find Sharn sitting on the floor
            with a muscular orc ass in front of her. You notice she's got two
            well lubed fingers slowly working the bumhole of her client.

            "Just in time" Sharn grunts as you enter "any moment now this stud
            here is going to empty his balls just from a little massage to the
            hidden spot up his ass"

            You watch as she expertly makes the orc cum with her fingers
            thinking you could put some of her technique to good use yourself
            back at the shop.

            Got 500 coins.

            Ass fingering action unlocked.
        `
    },
    {
        id: 'cumlubeOrder5',
        name: "Special Order - Cumlube",
        after: [ 'cumlubeOrder3' ],
        target: { cumlube: 5 },
        unlocks: ['buttplug'],
        reward: { coins: 500 },
        introContent: paragraphs`
            One of Madam Murbul's girls comes in with another order for 5
            bottles of cumlube.
        `,
        descriptionText: "Deliver 5 bottles of cumlube to Madam Murbul",
        completeContent: paragraphs`
            "We're running through our lube supply so quickly these days" says
            the Madam as she hands over a bag of coins in payment.

            "But business has never been better"

            "I got a delivery of equipment yesterday and now have more of these
            plugs they we can put to use, I guess you could have one"

            Got 500 coins, and a buttplug.

            Buttplug unlocked.
        `
    },
    {
        id: 'virilityPotionOrder1',
        name: "Special Order - Virility Potion",
        after: ['cumlubeOrder2'],
        target: { virilityPotion: 1 },
        unlocks: ['chastityCageUpgrade'],
        reward: { coins: 500 },
        introContent: paragraphs`
            Zarod, the village blacksmith, wants you to brew him a virility potion.
        `,
        descriptionText: "Deliver a Virility Potion to Zarod.",
        completeContent: paragraphs`
            "I found this old piece of paper lying around and turns it out it's part of the pattern for those cages, I made a couple already and it's a simple as snapping them in place."

            "It's a little collection tub, see?"

            Got 500 coins.

            Chastity Cage upgraded.
        `
    },
    {
        id: 'feyEssenceOrder1',
        name: "The magic of the Fey",
        target: { feyEssence: 1 },
        unlocks: ['passionPotion', 'rubyBark'],
        reward: { rubyBark: 2, coins: 100 },
        introContent: paragraphs`
            The warlock Urzul wants you to get her some Fey Essence.
        `,
        descriptionText: "Deliver 1 vial of Fey Essence to Urzul the warlock",
        completeContent: paragraphs`
            Got 100 coins and 2 ruby bark.

            Passion potion unlocked.
        `
    },
    {
        id: 'feyEssenceOrder2',
        name: "The magic of the Fey",
        after: ['feyEssenceOrder1'],
        target: { feyEssence: 1 },
        unlocks: ['lovePotion', 'sunpetalRoot'],
        reward: { sunpetalRoot: 2, coins: 100 },
        introContent: paragraphs`
            The warlock Urzul wants you to get her some Fey Essence.
        `,
        descriptionText: "Deliver 1 vial of Fey Essence to Urzul the warlock",
        completeContent: paragraphs`
            Got 100 coins and 2 sunpetal root.

            Love Potion unlocked.
        `
    },
    {
        id: 'feyEssenceOrder3',
        name: "The magic of the Fey",
        after: ['feyEssenceOrder1'],
        target: { feyEssence: 1 },
        unlocks: ['virilityPotion', 'milkyAlmonds'],
        reward: { milkyAlmonds: 2, coins: 100 },
        introContent: paragraphs`
            The warlock Urzul wants you to get her some Fey Essence.
        `,
        descriptionText: "Deliver 1 vial of Fey Essence to Urzul the warlock",
        completeContent: paragraphs`
            Got 100 coins and 2 milky almonds.

            Virility potion unlocked.
        `
    },
    {
        id: 'darkMagicRitual',
        name: "Dark Magic Ritual",
        after: ['feyEssenceOrder1', 'feyEssenceOrder2', 'feyEssenceOrder3'],
        target: { feyEssence: 20 },
        unlocks: [],
        reward: { coins: 2500 },
        introContent: paragraphs`
            The warlock Urzul needs a lot of Fey Essence for her next magic ritual.
        `,
        descriptionText: "Deliver 20 vials of Fey Essence to Urzul the warlock",
        completeContent: paragraphs`
            Got 2500 coins!

            Ooooh stuff!  TODO
        `
    }
])
