import Item from './item.js'
import Event from './event.js'
import {isControllingAction} from './elf.js'
import {createSelector, createStructuredSelector} from './reselect.js'
import {createCachedSelector, createStructuredCachedSelector} from './re-reselect.js'
import immutable from "./immutable.js"
const {List} = immutable

let checkItems = (wants, has) =>
    Object.entries(wants)
        .every(([key, amount]) => has[key] >= amount)

// Global selectors
let unlocked = state => state.unlocked

// Elf selectors
let entity = (state, {entityId}) => state.entities.get(entityId)
let entityCacheKey = (_state, {entityId}) => entityId
let createEntitySelector = selector => (state, props) => selector(entity(state, props))
let entityPropertySelectors = props => Object.fromEntries(
    props.map(prop => [
        prop,
        (state, {entityId}) => state.entities.get(entityId)[prop]
    ]))

let {
    id,
    arousal,
    climax,
    juice,
    action,
    isControlled,
    buttplug,
    chastityCage,
    collectedCum,
    collectedPrecum,
    spilledCum,
    spilledPrecum,
    cumlube,
    passionPotion,
    virilityPotion,
} = entityPropertySelectors([
    'id',
    'arousal',
    'climax',
    'juice',
    'action',
    'isControlled',
    'buttplug',
    'chastityCage',
    'collectedCum',
    'collectedPrecum',
    'spilledCum',
    'spilledPrecum',
    'cumlube',
    'passionPotion',
    'virilityPotion',
])

let collectLimit = createCachedSelector(
    isControlled,
    chastityCage,
    unlocked,
    (isControlled, chastityCage, unlocked) => {
        if (isControlled) {
            return 1000
        }
        if (chastityCage && unlocked.has('chastityCageUpgrade')) {
            return 200
        }
        return 0
    }
)(entityCacheKey)

let canStopAction = createEntitySelector(state => state.action !== 'idle')
let collectState = createCachedSelector(
    isControlled,
    collectLimit,
    collectedPrecum,
    collectedCum,
    (isControlled, collectLimit, collectedPrecum, collectedCum) => {
        let totalCollected = collectedPrecum + collectedCum
        if (totalCollected >= 10) {
            if (totalCollected > collectLimit) {
                return 'overflow'
            }
            return 'filled'
        }
        if (isControlled) {
            return 'empty'
        }
        return 'disabled'
    }
)(entityCacheKey)
let showCollected = (state, props) => collectState(state, props) != 'disabled'
let showOverflow = (state, props) => collectState(state, props) == 'overflow'
let showFilled = (state, props) => collectState(state, props) == 'filled'
let showEmpty = (state, props) => collectState(state, props) == 'empty'

let showSpilled = createEntitySelector(state => (state.spilledPrecum + state.spilledCum) > 1)

let collectedText = createCachedSelector(
    collectedCum,
    collectedPrecum,
    (collectedCum, collectedPrecum) =>
        `Collected ${Math.floor(collectedCum)} ml of cum and ${Math.floor(collectedPrecum)} ml of precum`
)(entityCacheKey)

let spilledText = createCachedSelector(
    spilledCum,
    spilledPrecum,
    (spilledCum, spilledPrecum) =>
        `There is ${Math.floor(spilledCum)} ml of cum and ${Math.floor(spilledPrecum)} ml of precum wasted on the floor`
)(entityCacheKey)

let showLube = createEntitySelector(state => state.saplube > 0 || state.cumlube > 0)
let lubeText = createCachedSelector(
    cumlube,
    cumlube => {
        let lubeName = cumlube > 0 ? 'Cumlube' : 'Slippery sap'
        return `Lubed up with ${lubeName}.`
    }
)(entityCacheKey)

let showPotion = createEntitySelector(state => state.passionPotion || state.virilityPotion)
let potionText = createCachedSelector(
    passionPotion,
    virilityPotion,
    (passionPotion, virilityPotion) => {
        let effects = []
        if (passionPotion) {
            effects.push("a passion potion")
        }
        if (virilityPotion) {
            effects.push("a virility potion")
        }
        if (effects.length > 0) {
            return "Under the effect of " + effects.join(', ')
        }
        return ""
    }
)(entityCacheKey)

let className = createEntitySelector(state =>
    state.isControlled ? 'entity controlled' : 'entity')

let assetName = createEntitySelector(state => {
    let variant = state.id % 4;
    return `content/elf${variant}.png`
})

let canFinger = createEntitySelector(state => !state.buttplug)
let canStroke = createEntitySelector(state => !state.chastityCage)

let showFinger = createCachedSelector(
    unlocked,
    unlocked => unlocked.has('finger')
)(entityCacheKey)

let showEdge = createCachedSelector(
    unlocked,
    unlocked => unlocked.has('edge')
)(entityCacheKey)


export let elfSelector = createStructuredCachedSelector({
    id,
    arousal,
    climax,
    juice,
    action,
    isControlled,
    buttplug,
    chastityCage,
    canStopAction,
    showCollected,
    showFilled,
    showEmpty,
    showSpilled,
    showOverflow,
    collectedText,
    spilledText,
    showLube,
    lubeText,
    showPotion,
    potionText,
    className,
    assetName,
    canFinger,
    canStroke,
    showFinger,
    showEdge,
    // For effects
    collectedCum,
    collectedPrecum,
    spilledCum,
    spilledPrecum,
})(entityCacheKey)

// Game state selectors
let turn = state => state.turn
let win = state => state.items.win
let precum = state => state.items.precum
let cum = state => state.items.cum
let coins = state => state.coins
let event = state => state.event

let cumText = state => `${Math.round(state.items.cum)} ml cum`
let precumText = state => `${Math.round(state.items.precum)} ml pre`

let craftingClassName = state => state.crafting ? 'crafting-status active' : 'crafting-status'

let craftingStatusText = state => {
    if (!state.crafting) {
        return 'Not crafting'
    }
    let craftingName = Item[state.crafting].name
    return `Crafting ${craftingName}`
}

let craftingProgress = state => {
    if (!state.crafting) {
        return 0;
    }
    return (state.craftingProgress + 1) / state.craftingTarget
}

let canBuyElf = state => state.coins >= 500
let showSidebar = state => state.sidebar != null
let showMarketMenu = state => state.sidebar == 'marketMenu'
let showCraftingMenu = state => state.sidebar == 'craftingMenu'
let showInventoryMenu = state => state.sidebar == 'inventoryMenu'
let showEventMenu = state => state.sidebar == 'eventMenu'
let playerActive = state => state.focusedEntity && state.entities.get(state.focusedEntity).isControlled
let playerClassName = createSelector(
    playerActive,
    playerActive => playerActive ? 'player active' : 'player'
);

let items = state => state.items

let unlockedItems = createSelector(
    unlocked,
    unlocked => Object.values(Item).filter(item => unlocked.has(item.id))
)

let craftItems = createSelector(
    unlockedItems,
    items,
    (unlocked, items) => List(
        unlocked
            .filter(item => item.ingredients)
            .map(item => ({
                id: item.id,
                amount: items[item.id],
                name: item.name,
                descriptionText: item.descriptionText,
                canCraft: checkItems(item.ingredients, items),
                ingredientsText: (
                    "Ingredients: " + Object.entries(item.ingredients)
                    .map(([key, amount]) => {
                        let name = Item[key]?.name || key
                        return `${amount} ${name}`
                    })
                    .join(", ")
                ),
            }))
    )
)

let buyItems = createSelector(
    unlockedItems,
    items,
    coins,
    (unlocked, items, coins) => List(
        unlocked
            .filter(item => item.buyValue)
            .map(item => ({
                id: item.id,
                amount: items[item.id],
                name: item.name,
                buyValue: item.buyValue,
                descriptionText: item.descriptionText,
                canBuy: coins >= item.buyValue,
            }))
    )
)

let sellItems = createSelector(
    unlockedItems,
    items,
    (unlocked, items) => List(
        unlocked
            .filter(item => item.sellValue)
            .map(item => ({
                id: item.id,
                amount: items[item.id],
                name: item.name,
                sellValue: item.sellValue,
                descriptionText: item.descriptionText,
            }))
    )
)

let inventoryItems = createSelector(
    unlockedItems,
    items,
    (unlocked, items) => List(
        unlocked
            .map(item => ({
                id: item.id,
                amount: items[item.id],
                name: item.name,
                usable: item.usable,
                descriptionText: item.descriptionText,
            }))
    )
)

let eventDescriptionText = createSelector(
    event,
    event => event ? Event[event].descriptionText : "No event"
)

let canCompleteEvent = createSelector(
    event,
    items,
    (event, items) => event && checkItems(Event[event].target, items)
)

let entities = state =>
    state.entities.map(entity => elfSelector(state, {entityId: entity.id}))

export let rootSelector = createStructuredSelector({
    buyItems,
    canBuyElf,
    canCompleteEvent,
    coins,
    craftingClassName,
    craftingStatusText,
    craftingProgress,
    craftItems,
    cumText,
    entities,
    event,
    eventDescriptionText,
    inventoryItems,
    precum,
    precumText,
    sellItems,
    showCraftingMenu,
    showEventMenu,
    showInventoryMenu,
    showMarketMenu,
    showSidebar,
    turn,
    win,
    playerClassName,
})
