import immutable from "./immutable.js"
const {Record, List} = immutable

let randNormal = (min, max, skew) => {
    // Box-Muller transform
    let u = 0, v = 0
    while (u === 0) u = Math.random()
    while (v === 0) v = Math.random()
    let num = Math.sqrt(-2.0 * Math.log(u)) * Math.cos(2.0 * Math.PI * v)
    num = num / 10.0 + 0.5
    if (num > 1 || num < 0)
        num = randNormal()

    return Math.pow(num, skew) * (max - min) + min
}

let Ease = {
    outCubic: t => Math.pow(1 - t, 3),
    outSquare: t => Math.pow(1 - t, 2),
    inCubic: t => Math.pow(t, 3),
    inSquare: t => Math.pow(t, 2),
}

let grow = (current, amount, max = 100) =>
    Math.min(
        max,
        current + amount * Math.max(Ease.outSquare(Math.max(current, 1) / 120), 0.1)
    )

let shrink = (current, amount) =>
    Math.max(
        0,
        current - amount * Math.max(Ease.outSquare(Math.max(100 - current, 1) / 120), 0.1)
    )

let randTarget = (value, min, dependency) =>
    value >= min
        ? Ease.inCubic(1 - (100 - value) / (100 - min)) * dependency
        : 0

let randCheck = (value, min, dependency) =>
    Math.random() < randTarget(value, min, dependency)

let collectLimit = (rootState, state) => {
    if (state.isControlled) {
        return 1000
    }
    if (state.chastityCage && rootState.unlocked.has('chastityCageUpgrade')) {
        return 200
    }
    return 0
}

export let isControllingAction = (action) =>
    ['idle', 'masturbate'].indexOf(action) == -1

export let isCockAction = (action) =>
    ['edge', 'stroke', 'masturbate'].indexOf(action) !== -1

export let isAnalAction = (action) =>
    ['finger'].indexOf(action) !== -1

export default class Elf extends Record({
    id: 0,
    arousal: 0.0,
    thresArousal: 80.0,
    climax: 0.0,
    juice: 20.0,
    action: 'idle',
    collectedCum: 0.0,
    collectedPrecum: 0.0,
    spilledCum: 0.0,
    spilledPrecum: 0.0,
    hesitation: 0.0,
    actionDuration: 0,
    cumStep: 0,
    controlled: 0,
    exhaustion: 0,
    pressure: 0,
    buttplug: false,
    chastityCage: false,
    saplube: 0,
    cumlube: 0,
    passionPotion: 0,
    virilityPotion: 0,
}, 'Elf') {
    tick (rootState) {
        return this.merge({
            arousal: this.nextArousal(),
            juice: this.nextJuice(),
            climax: this.nextClimax(),
            pressure: this.nextPressure(),
            ...this.nextAction(),
            controlled: Math.max(this.controlled - 1, 0),
            hesitation: Math.max(this.hesitation - 1, 0),
            exhaustion: Math.max(this.exhaustion - 1, 0),
            passionPotion: Math.max(this.passionPotion - 1, 0),
            virilityPotion: Math.max(this.virilityPotion - 1, 0),
        })
            .applyAction()
            .maybePrecum(rootState)
            .maybeCum(rootState)
    }

    setAction (action, actionDuration = 20) {
        if (this.chastityCage && isCockAction(action)) {
            return this
        }
        if (this.buttplug && isAnalAction(action)) {
            return this
        }
        return this.merge({ action, actionDuration })
    }

    addItem (item) {
        switch (item) {
            case 'cumlube':
                return this.merge({
                    action: 'idle',
                    hesitation: 5,
                    cumlube: this.cumlube + 100,
                })
            case 'slipperySap':
                return this.merge({
                    action: 'idle',
                    hesitation: 5,
                    saplube: this.saplube + 100,
                })
            case 'passionPotion':
                return this.merge({
                    action: 'idle',
                    hesitation: 5,
                    passionPotion: this.passionPotion + 500,
                })
            case 'virilityPotion':
                return this.merge({
                    action: 'idle',
                    hesitation: 5,
                    virilityPotion: this.virilityPotion + 500,
                })
            default:
                return this.merge({
                    action: 'idle',
                    hesitation: 5,
                    [item]: true,
                })
        }
    }

    removeItem (item) {
        return this.merge({
            action: 'idle',
            hesitation: 5,
            [item]: false
        })
    }

    // When the Orc looks away
    lostFocus () {
        if (isControllingAction(this.action)) {
            return this.merge({
                action: 'idle',
                hesitation: 10,
                controlled: 0,
                collectedCum: 0,
                collectedPrecum: 0,
            })
        }
        return this.merge({
            controlled: 0,
            collectedCum: 0,
            collectedPrecum: 0,
        })
    }

    focus () {
        return this.set('controlled', 50)
    }

    clearCollected () {
        return this.merge({
            collectedCum: 0,
            collectedPrecum: 0,
        })
    }

    clearSpilled () {
        return this.merge({
            spilledCum: 0,
            spilledPrecum: 0,
        })
    }

    // Apply the active action
    applyAction () {
        // Only apply every other tick to get a bouncing effect
        if (this.actionDuration % 2) {
            return this;
        }
        switch (this.action) {
            case 'masturbate':
                return this.pleasure(15, 2)
            case 'stroke':
                return this.pleasure(20, 4)
            case 'edge':
                if (this.climax > 84 || this.cumStep > 0) {
                    return this.merge({
                        action: 'idle',
                        hesitation: 35,
                        controlled: 50,
                    })
                } else {
                    return this.pleasure(30, 4)
                }
            case 'tease':
                return this.pleasure(8, 8)
            case 'finger':
                return this.pleasure(4, 8)
                    .merge({ pressure: grow(this.pressure, 12) })
            case 'slap':
                return this.merge({
                    climax: shrink(this.climax, 20),
                    arousal: shrink(this.arousal, 40),
                    hesitation: 50,
                    controlled: 50,
                    action: 'idle',
                    actionDuration: 0,
                })
            case 'idle':
            case 'collect':
                return this;
            default:
                console.log('Unknown action', this.action)
                return this.set('action', 'idle')
        }
    }

    // Passive growth of arousal
    nextArousal () {
        let arousal = this.arousal

        arousal -= this.exhaustion / 10

        if (arousal > this.thresArousal + (this.climax / 5)) {
            arousal -= Math.random() * 5
        }

        let increase = (
            Math.max((this.juice - arousal) / 4, 0) +
            this.juice / 20 +
            Math.max((this.climax - 20) / 20, 0) +
            (this.buttplug ? 5 : 0)
        )
        if (this.passionPotion) {
            increase *= 1.5
        }
        return grow(arousal, increase)
    }

    // Passive growth of climax
    nextClimax () {
        let climax = Math.max(this.climax - Math.max(this.exhaustion / 10, 1), 0)
        if (this.buttplug) {
            return grow(climax, 2)
        }
        return climax
    }

    // Passive growth of ball juice
    nextJuice () {
        let increase = (
            (this.virilityPotion ? 5 : 0.1) +
            Math.max((this.climax - 60) / 100, 0) +
            Math.max((this.arousal - 60) / 100, 0)
        )
        return grow(this.juice, increase)
    }

    // Passive growth of ball pressure
    nextPressure () {
        let pressure = Math.max(this.pressure - Math.max(this.exhaustion / 10, 2), 0)
        let increase = (
            Math.max(this.juice - 10, 0) / 4 +
            this.climax / 25 +
            this.arousal / 32
        )
        return grow(pressure, increase)
    }

    // Tick down duration, pick new action if possible
    nextAction () {
        let actionDuration = Math.max(this.actionDuration - 1, 0)
        if (this.action != 'idle' && actionDuration == 0) {
            return {
                actionDuration,
                action: 'idle',
                controlled: isControllingAction(this.action) ? 50 : 0
            }
        }
        if (this.action != 'idle' || this.hesitation > 0 || this.chastityCage) {
            return { actionDuration }
        }
        let target = randTarget(this.arousal, 20, 0.5)
        if (Math.random() < target) {
            return { action: 'masturbate', actionDuration: 500, controlled: 0 }
        }
    }

    pleasure (climaxIncrease, arousalIncrease) {
        let climaxBonus = this.arousal
        let arousalBonus = 0
        let cumlube = this.cumlube
        let saplube = this.saplube
        if (cumlube > 0) {
            cumlube -= 1
            climaxBonus += 50
            arousalBonus += 50
        } else if (this.saplube > 0) {
            saplube -= 1
            climaxBonus += 40
        }
        return this.merge({
            cumlube,
            saplube,
            climax: grow(this.climax, climaxIncrease * (1 + climaxBonus / 100)),
            arousal: grow(this.arousal, arousalIncrease * (1 + arousalBonus / 100))
        })
    }

    // Leak precum when close or when balls are full
    maybePrecum (rootState) {
        let target = randTarget(this.juice / 2 + this.pressure / 2 + this.arousal / 15 + this.climax / 7, 10, 0.6)
        if (Math.random() < target) {
            let precum = this.juice / 100
            if (this.arousal > 60)
                precum *= (this.arousal - 40) / 30
            if (this.climax > 20)
                precum *= this.climax / 30
            let limit = collectLimit(rootState, this)
            let collected = Math.min(precum, limit)
            let spilled = precum - collected
            return this.merge({
                juice: this.juice - precum / 20,
                collectedPrecum: this.collectedPrecum + collected,
                spilledPrecum: this.spilledPrecum + spilled,
            })
        }
        return this;
    }

    cumShot (rootState) {
        let volume = 1 + Math.random() * Math.min(Math.max(this.juice / 7 - 1), 7)
        let newJuice = Math.max(this.juice - volume, 0)
        let limit = collectLimit(rootState, this)
        let adjVolume = volume * 4
        let collected = Math.min(adjVolume, limit)
        let spilled = adjVolume - collected
        return this.merge({
            collectedCum: this.collectedCum + collected,
            spilledCum: this.spilledCum + spilled,
            juice: newJuice,
            cumStep: this.cumStep + 1,
            climax: shrink(this.climax, 10),
            pressure: this.pressure - volume * 2,
            exhaustion: this.exhaustion + 10 + 50 * Ease.inCubic(this.climax / 100),
            controlled: Math.max(this.controlled, this.isControlled ? 5 : 0),
        })
    }

    // Orgasm when climax is reached
    maybeCum (rootState) {
        let adjustedClimax = grow(this.climax, this.pressure)
        if (this.cumStep > 0) {
            let climaxTarget = randTarget(100 - adjustedClimax, 0, 1.0)
            let pressureTarget = randTarget(100 - this.pressure, 40, 1.0)
            let stopTarget = (climaxTarget + pressureTarget) * Ease.inSquare(Math.min(this.cumStep / 5, 1.0))
            if (this.juice < 1 || Math.random() < stopTarget) {
                return this.merge({
                    cumStep: 0,
                    hesitation: 10,
                    action: isControllingAction(this.action) ? this.action : 'idle',
                })
            }
            return this.cumShot(rootState)
        }
        if (randCheck(adjustedClimax, 90, 0.9)) {
            return this.cumShot(rootState)
        }
        if (randCheck(this.pressure, 80, 1.0)) {
            return this.cumShot(rootState)
        }
        return this;
    }

    get isControlled () {
        return this.controlled > 0 || isControllingAction(this.action)
    }

    get hasManualAction () {
        return !isControllingAction(this.action)
    }
}
