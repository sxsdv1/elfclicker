import immutable from "./immutable.js"
const {Record, List, Set} = immutable
import Elf from './elf.js'
import Item from './item.js'
import Event from './event.js'
import {getDataBindings, findDataBindings, syncDisplayProperties} from './databinding.js'
import {rootSelector} from './selectors.js'

let Items = Record({
    precum: 0,
    cum: 0,
    cumlube: 0,
    feyEssence: 0,
    buttplug: 0,
    passionPotion: 0,
    lovePotion: 0,
    virilityPotion: 0,
    chastityCage: 0,
    slipperySap: 0,
    alcohol: 0,
    rubyBark: 0,
    milkyAlmonds: 0,
    sunpetalRoot: 0,
    win: 0,
}, 'Items')

class ElfClickerState extends Record({
    turn: 0,
    coins: 400,
    entities: List(),
    focusedEntity: null,
    sidebar: null,
    crafting: null,
    craftingProgress: null,
    craftingTarget: null,
    event: null,
    unlocked: Set.of('feyEssence', 'cumlube', 'slipperySap', 'alcohol', 'win'),
    completedEvents: Set(),
    items: Items(),
}, 'ElfClickerState') {

    tick () {
        return this.merge({
            turn: this.turn + 1,
            entities: this.entities.map(ent => ent.tick(this)),
            event: this.nextEvent(),
            ...this.nextCrafting(),
        })
    }

    nextEvent () {
        if (this.event || this.turn < 100 || this.turn % 50 != 0) {
            return this.event
        }
        let availableEvents = Object.values(Event)
            .filter(event => !this.completedEvents.has(event.id))
            .filter(event =>
                (event.after || []).every(after => this.completedEvents.has(after)))
            .filter(event =>
                Object.keys(event.target)
                    .every(item => this.unlocked.has(item)))
        if (availableEvents.length > 0) {
            return availableEvents[Math.floor(Math.random() * availableEvents.length)].id
        }
        return null
    }

    nextCrafting () {
        if (!this.crafting) {
            return {}
        }
        let craftingProgress = this.craftingProgress + 1
        if (craftingProgress < this.craftingTarget) {
            return { craftingProgress }
        }

        console.log("Crafted", this.crafting)
        return {
            crafting: null,
            craftingProgress: null,
            craftingTarget: null,
            items: this.items.set(this.crafting, this.items[this.crafting] + 1),
        }
    }

    setCrafting (recipe) {
        let newAmounts = {}
        for (let [key, amount] of Object.entries(Item[recipe].ingredients)) {
            if (this.items[key] < amount) {
                console.log("Can't make", recipe)
                return this
            }
            newAmounts[key] = this.items[key] - amount
        }
        return this.merge({
            crafting: recipe,
            craftingProgress: 0,
            craftingTarget: Item[recipe].time,
            items: this.items.merge(newAmounts),
        })
    }

    sellItem (item) {
        return this.merge({
            items: this.items.set(item, this.items[item] - 1),
            coins: this.coins + Item[item].sellValue
        })
    }

    buyItem (item) {
        return this.merge({
            items: this.items.set(item, this.items[item] + 1),
            coins: this.coins - Item[item].buyValue
        })
    }

    useItem (item, entityIdx) {
        if (this.entities.get(entityIdx)[item]) {
            return this
        }
        return this.merge({
            focusedEntity: entityIdx,
            items: this.items.set(item, this.items[item] - 1),
            entities: this.entities.update(entityIdx, ent => ent.addItem(item)),
        })
    }

    removeItem (item, entityIdx) {
        if (!this.entities.get(entityIdx)[item]) {
            return this
        }
        return this.merge({
            focusedEntity: entityIdx,
            items: this.items.set(item, this.items[item] + 1),
            entities: this.entities.update(entityIdx, ent => ent.removeItem(item)),
        })
    }

    completeEvent () {
        let event = Event[this.event]
        let newAmounts = {}
        for (let [key, amount] of Object.entries(event.target)) {
            if (this.items[key] < amount) {
                console.log("Can't complete event")
                return this
            }
            newAmounts[key] = this.items[key] - amount
        }
        for (let [key, amount] of Object.entries(event.reward)) {
            if (key !== 'coins') {
                newAmounts[key] = this.items[key] + amount
            }
        }
        return this.merge({
            event: null,
            sidebar: null,
            coins: this.coins + (event.reward.coins || 0),
            completedEvents: this.completedEvents.add(event.id),
            unlocked: this.unlocked.union(Set(event.unlocks)),
            items: this.items.merge(newAmounts),
        })
    }
}

export function init () {
    return new ElfClickerState({
        entities: List.of(new Elf({ id: 0, juice: 60 }))
    })
}

export function hydrate (jsonState) {
    return new ElfClickerState({
        ...jsonState,
        unlocked: jsonState.unlocked ? Set(jsonState.unlocked) : undefined,
        completedEvents: Set(jsonState.completedEvents),
        items: (jsonState.items ? Items(jsonState.items) : (Items({
            cum: jsonState.cum,
            precum: jsonState.precum,
            cumlube: jsonState.cumlube,
            feyEssence: jsonState.feyEssence,
            buttplug: jsonState.buttplug,
            passionPotion: jsonState.passionPotion,
            chastityCage: jsonState.chastityCage,
            slipperySap: jsonState.slipperSap,
            alcohol: jsonState.alochol,
            rubyBark: jsonState.rubyBark,
            win: jsonState.win,
        }))),
        entities: List(jsonState.entities.map((ent, i) =>
            new Elf({ ...ent, id: i }))) // Override to fix broken saved state
    })
}

export function tick (state) {
    return state.tick()
}

export function setAction (state, entityIdx, action, data) {
    // Take all collected cum and stop current action when switching focus
    if (state.focusedEntity && state.focusedEntity != entityIdx) {
        let entity = state.entities.get(state.focusedEntity)
        state = state.merge({
            items: state.items.merge({
                cum: state.items.cum + entity.collectedCum,
                precum: state.items.precum + entity.collectedPrecum,
            }),
            entities: state.entities.update(
                state.focusedEntity, entity => entity.lostFocus()),
        })
    }

    switch (action) {
        case 'collect':
            return state.merge({
                focusedEntity: entityIdx,
                entities: state.entities.update(entityIdx, ent => ent.focus()),
            })
        case 'pickup':
            return state.merge({
                items: state.items.merge({
                    cum: state.items.cum + state.entities.get(entityIdx).collectedCum,
                    precum: state.items.precum + state.entities.get(entityIdx).collectedPrecum,
                }),
                focusedEntity: entityIdx,
                entities: state.entities.update(entityIdx, ent => ent.clearCollected()),
            })
        case 'clean':
            return state.merge({
                focusedEntity: entityIdx,
                entities: state.entities.update(entityIdx, ent => ent.clearSpilled()),
            })
        case 'sell_item':
            return state.sellItem(data.item)
        case 'buy_item':
            return state.buyItem(data.item)
        case 'use_item':
            return state.useItem(data.item, entityIdx)
        case 'remove_item':
            return state.removeItem(data.item, entityIdx)
        case 'buy_elf':
            return state.merge({
                coins: state.coins - 500,
                entities: state.entities.push(new Elf({ id: state.entities.size }))
            })
        case 'marketMenu':
        case 'craftingMenu':
        case 'inventoryMenu':
        case 'eventMenu':
            return state.merge({
                sidebar: (state.sidebar == action ? null : action)
            })
        case 'craft':
            if (state.crafting) {
                return state
            }
            return state.setCrafting(data.recipe)
        case 'complete_event':
            return state.completeEvent()
        case 'stop_action':
            action = state.entities.get(entityIdx).hasManualAction ? 'slap' : 'idle'
            // fall through
        default:
            return state.merge({
                focusedEntity: entityIdx,
                entities: state.entities.update(entityIdx, ent => ent.setAction(action))
            })
    }
}

let showEffects = (root, effects) => {
    for (let el of root.querySelectorAll('[data-effect]')) {
        if (effects[el.dataset.effect]) {
            el.style.animation = 'none'
            el.offsetHeight // trigger reflow
            el.style.animation = null
        }
    }
}

let display = (root, bindings, oldState, newState) => {
    let entityElements = root.querySelectorAll('.entity')
    newState.entities.forEach((newEntity, i) => {
        let elem = entityElements[i]
        let oldEntity = oldState && oldState.entities.get(i)
        if (oldEntity) {
            showEffects(elem, {
                cum: (newEntity.collectedCum > oldEntity.collectedCum ||
                    newEntity.spilledCum > oldEntity.spilledCum),
                precum: (newEntity.collectedPrecum > oldEntity.collectedPrecum ||
                    newEntity.spilledPrecum > oldEntity.spilledPrecum),
                slap: (newEntity.action == 'slap' && oldEntity.action != 'slap')
            })
        }
    })
    syncDisplayProperties(bindings, oldState, newState)
}

export function createDisplay (root) {
    let oldProps = null
    let bindings = findDataBindings(root)
    return (newState) => {
        let newProps = rootSelector(newState)
        display(root, bindings, oldProps, newProps)
        oldProps = newProps
    }
}
